﻿using System.Threading.Tasks;

namespace FileServerTestTask.BackEnd
{
    /// <summary>
    /// Инкапсулирает работу с файлами на диске
    /// </summary>
    public interface IDefaultFileReader
    {
        public Task<object> ReadAsync(string fileName);
    }
}
