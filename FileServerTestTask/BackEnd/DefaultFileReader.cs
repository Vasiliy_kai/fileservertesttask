﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace FileServerTestTask.BackEnd
{
    /// <summary>
    /// Управляет чтением файлов с диска
    /// </summary>
    public class DefaultFileReader : IDefaultFileReader
    {
        private const int DelayToRetryMilSec = 10000;
        private const int DelayToGetMilSec = 2000;

        private readonly string folderPath;
        private readonly IDictionary<string, object> openFiles;
        private readonly IEnumerable<string> readingFiles;

        /// <summary>
        /// Конструктор
        /// </summary>
        public DefaultFileReader()
        {
            folderPath = @"..\FileServerTestTask\BackEnd\Files\";
            openFiles = new Dictionary<string, object>();
            readingFiles = new List<string>();
        }

        /// <summary>
        /// Прочитать файл
        /// </summary>
        /// <param name="fileName">Имя файла</param>
        /// <returns></returns>
        public async Task<object> ReadAsync(string fileName)
        {
            var result = string.Empty;
  
            while (readingFiles.Contains(fileName))
            {
                Thread.Sleep(DelayToRetryMilSec);
            }

            if (openFiles.ContainsKey(fileName))
            {
                Thread.Sleep(DelayToGetMilSec);
                result = ((Dictionary<string, object>)openFiles).GetValueOrDefault(fileName).ToString();
            }
            else
            {
                ((List<string>)readingFiles).Add(fileName);
                result = (await ReadFile(folderPath + fileName)).ToString();
                ((List<string>)readingFiles).Remove(fileName);
                openFiles.Add(fileName, result);
            }

            return result;
        }

        private async Task<object> ReadFile(string fileName)
        {
            var result = string.Empty;
            using (var streamReader = new StreamReader(fileName))
            {
                result = await streamReader.ReadToEndAsync();
            }

            return result;
        }
    }
}
