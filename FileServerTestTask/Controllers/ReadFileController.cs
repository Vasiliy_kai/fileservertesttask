﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FileServerTestTask.BackEnd;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace FileServerTestTask.Controllers
{
    [Authorize]
    [ApiController]
    [Route("[controller]")]
    public class ReadFileController : ControllerBase
    {
        private readonly ILogger<ReadFileController> _logger;
        private readonly IDefaultFileReader _reader;

        public ReadFileController(IDefaultFileReader reader, ILogger<ReadFileController> logger)
        {
            _reader = reader;
        }

        [HttpGet]
        public async Task<object> Get(string fileName)
        {
            var result = string.Empty;
            try
            {
                result = (await _reader.ReadAsync(fileName)).ToString();
            }
            catch(Exception e)
            {
                result = e.Message;
            }

            return result;
        }
    }
}
